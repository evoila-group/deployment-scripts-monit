#!/bin/bash

export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-monit/raw/HEAD/monit"

wget $REPOSITORY_MONIT/monit-template.sh --no-cache
chmod +x monit-template.sh
./monit-template.sh -u evoila -p evoila
